<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$mod_strings = Array(
	'Assessments' => 'Assessments',
	'Name' => 'Name',
	'SINGLE_Assessments' => 'Assessment',
	'Assessment ID' => 'Assessments ID',
	'Assessment No' => 'Assessment No',
	'Assessment Date' => 'Assessment Date',
	'Employees' => 'Employees',
	'Candidates' => 'Candidates',
	'Participation' => 'Participation',
	'Notes' => 'Notes',
	'Correct Answers' => 'Correct Answers',
	'Answers %' => 'Answers %',
	'Total Answers' => 'Total Answers',

	'LBL_ASSESSMENT_INFORMATION' => 'Assessment Information',
	'LBL_PERSONALIZED_INFORMATION' => 'Personalized Information',
	'LBL_DESCRIPTION_INFORMATION' => 'Description',


);

?>
